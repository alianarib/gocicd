package handler

import (
	"fmt"
	"net/http"
	"os"
)

func Hello(w http.ResponseWriter, r *http.Request) {
	if r.URL.Query().Get("test") == "test" {
		panic("Mock internal server error")
	}

	fmt.Fprintln(w, "Hello, Indonesia! My name is:", os.Getenv("MYNAME"))
}

func Goodbye(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Goodbye, Indonesia! My name is:", os.Getenv("MYNAME"))
}
